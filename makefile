COMPILER = nvcc

EXEC_FILES = cuda_td_1.exe cuda_td_2.exe cuda_td_2_v2.exe cuda_td_3.exe cuda_td_4.exe cuda_td_5.exe
EXEC_FILES = cuda_td_5_vcublas.exe

%_vcublas.exe: %_vcublas.cu
	$(COMPILER) $< -o $@ -lcublas
%.exe: %.cu
	$(COMPILER) $< -o $@

exec: $(EXEC_FILES)
	@echo "\nExo 1, Addition de vecteurs:"
	./cuda_td_1.exe dataset/vector/input0.raw dataset/vector/input1.raw dataset/vector/output.raw
	@echo "\nExo 2, Image en noir et blanc:"
	./cuda_td_2.exe dataset/images/logo-insa-lyon-p6.ppm dataset/images/logo-insa-lyon-nb.ppm
	@echo "\nExo 2, Image en noir et blanc (avec la shared):"
	./cuda_td_2_v2.exe dataset/images/logo-insa-lyon-p6.ppm dataset/images/logo-insa-lyon-nb.ppm
	@echo "\nExo 3, Floutage d'image:"
	./cuda_td_3.exe	dataset/images/logo-insa-lyon-p6.ppm dataset/images/logo-insa-lyon-blur.ppm
	@echo "\nExo 4, Multiplication de matrices:"
	./cuda_td_4.exe dataset/matrix/input0.raw dataset/matrix/input1.raw dataset/matrix/output.raw
	@echo "\nExo 5, Multiplication de matrices (avec shared):"
	./cuda_td_5.exe dataset/matrix/input0.raw dataset/matrix/input1.raw dataset/matrix/output.raw
	@echo "\nExo 5, Multiplication de matrices (avec cublas):"
	./cuda_td_5_vcublas.exe dataset/matrix/input0.raw dataset/matrix/input1.raw dataset/matrix/output.raw
