#include "wb.h"

#define wbCheck(stmt)\
do {\
    cudaError_t err = stmt;\
    if (err != cudaSuccess) {\
        wbLog(ERROR, "Failed to run stmt ", #stmt);\
        wbLog(ERROR, "Got CUDA error ... ", cudaGetErrorString(err));\
        return -1;\
    }\
} while (0)

// profondeur de la shared
#define S 64

// Compute C = A * B
__global__ void matrixMultiplyShared(float *A, float *B, float *C, int M, int N, int K) {
  // On suppose des blocs de 32 x 32 ( 1024 threads )
  __shared__ float sA[ 32 * S ];
  __shared__ float sB[ S * 32 ];
  
  int idx = blockDim.x * blockIdx.x + threadIdx.x; // col
  int idy = blockDim.y * blockIdx.y + threadIdx.y; // row

     // idx locaux pour charger la mémoire partagée
  int idLAx = threadIdx.y;
  int idLAy = threadIdx.x;
  int idLBx = threadIdx.x;
  int idLBy = threadIdx.y;

  int idGBlockM = blockIdx.y * blockDim.y;
  int idGBlockN = blockIdx.x * blockDim.x;
  int idGBlockK = 0;
  
  float res = 0.f;
  for (int k0 = 0; k0 < K; k0 += S){
    // Load (on suppose que S%32 == 0
    for (int s = 0; s < S; s+=32){
        sA[ (idLAx + s) + idLAy * S ] = A[ (idGBlockK + idLAx + s) + (idGBlockM + idLAy) * K ];
        sB[ idLBx + (idLBy + s) * 32 ] = B[ (idGBlockN + idLBx) + (idGBlockK + idLBy + s) * N ];
    } 
    idGBlockK += S;
    __syncthreads();

    // Compute
    for (int k1 = 0; k1 < S && k0 + k1 < K; k1 += 1){
        res += sA[ threadIdx.y * S + k1 ] * sB[ threadIdx.x + k1 * 32 ];
    }
  }
  C[ idy * N + idx ] = res;
}

int main(int argc, char **argv) {
    wbArg_t args;
    float *hostA; // The A matrix
    float *hostB; // The B matrix
    float *hostC; // The output C matrix
    float *deviceA;
    float *deviceB;
    float *deviceC;
    int numARows; // number of rows in the matrix A
    int numAColumns; // number of columns in the matrix A
    int numBRows; // number of rows in the matrix B
    int numBColumns; // number of columns in the matrix B
    int numCRows; // number of rows in the matrix C (you have to set this)
    int numCColumns; // number of columns in the matrix C (you have to set this)
    args = wbArg_read(argc, argv);
    wbTime_start(Generic, "Importing data and creating memory on host");
    hostA = (float *)wbImport(wbArg_getInputFile(args, 0), &numARows, &numAColumns);
    hostB = (float *)wbImport(wbArg_getInputFile(args, 1), &numBRows, &numBColumns);

    //@@ Set numCRows and numCColumns
    numCRows = numARows;
    numCColumns = numBColumns;

    //@@ Allocate the hostC matrix
    hostC = (float*) malloc( sizeof(float) * numCRows * numCColumns );    
	
    wbTime_stop(Generic, "Importing data and creating memory on host");
    wbLog(TRACE, "The dimensions of A are ", numARows, " x ", numAColumns);
    wbLog(TRACE, "The dimensions of B are ", numBRows, " x ", numBColumns);
    wbTime_start(GPU, "Allocating GPU memory.");

    //@@ Allocate GPU memory here
    cudaMalloc( (void**) &deviceA, sizeof(float) * numARows * numAColumns );
    cudaMalloc( (void**) &deviceB, sizeof(float) * numBRows * numBColumns );
    cudaMalloc( (void**) &deviceC, sizeof(float) * numCRows * numCColumns );

    wbTime_stop(GPU, "Allocating GPU memory.");
    wbTime_start(GPU, "Copying input memory to the GPU.");
 
    //@@ Copy memory to the GPU here
    cudaMemcpy( deviceA, hostA, sizeof(float) * numARows * numAColumns, cudaMemcpyHostToDevice );   
    cudaMemcpy( deviceB, hostB, sizeof(float) * numBRows * numBColumns, cudaMemcpyHostToDevice );   

    wbTime_stop(GPU, "Copying input memory to the GPU.");
	
    //@@ Initialize the grid and block dimensions here
    dim3 blockDim = { 32, 32, 1 };
    dim3 gridDim = { (numCColumns + blockDim.x - 1 ) / blockDim.x, 
                     (numCRows + blockDim.y - 1 ) / blockDim.y, 1 };
					 
    wbTime_start(Compute, "Performing CUDA computation");

    //@@ Launch the GPU Kernel here
    matrixMultiplyShared<<<gridDim, blockDim>>>( deviceA, deviceB, deviceC, numARows, numBColumns, numAColumns);
    
    cudaDeviceSynchronize();
    wbTime_stop(Compute, "Performing CUDA computation");
    wbTime_start(Copy, "Copying output memory to the CPU");

    //@@ Copy the GPU memory back to the CPU here
    cudaMemcpy( hostC, deviceC, sizeof(float) * numCRows * numCColumns, cudaMemcpyDeviceToHost );   

    wbTime_stop(Copy, "Copying output memory to the CPU");
    wbTime_start(GPU, "Freeing GPU Memory");

    //@@ Free the GPU memory here
    cudaFree( deviceA );
    cudaFree( deviceB );
    cudaFree( deviceC );
	
    wbTime_stop(GPU, "Freeing GPU Memory");
    wbSolution(args, hostC, numCRows, numCColumns);
    free(hostA);
    free(hostB);
    free(hostC);
    return 0;
}
